module WebAPISpec where

import           Data.Aeson
import           Data.ByteString                ( ByteString )
import qualified Data.ByteString.Lazy          as LBS
import           Data.ByteString.Lazy.Char8     ( unpack )
import           Data.IORef                     ( newIORef )
import qualified Data.Map.Strict               as M
import           Data.String                    ( fromString )
import           Network.HTTP.Types             ( hContentType
                                                , methodPost
                                                )
import           Network.Wai.Test               ( SResponse )
import qualified System.Random                 as R
import           Test.Hspec                     ( describe
                                                , it
                                                , Spec
                                                )
import           Test.Hspec.Wai                 ( get
                                                , request
                                                , ResponseMatcher(..)
                                                , shouldRespondWith
                                                , WaiSession
                                                , with
                                                )


import           InterfaceImplementation.App    ( createApp )
import           Config
import           LogicInterface.Types


testConf :: Config
testConf = MkConfig 5

-- | Sends a POST request with a JSON body
postJSON :: ByteString -> LBS.ByteString -> WaiSession st SResponse
postJSON path = request methodPost path [(hContentType, "application/json")]


webAPISpec :: Spec
webAPISpec = with
  (do
    m1 <- newIORef M.empty
    m2 <- newIORef M.empty
    g  <- newIORef (R.mkStdGen 0)
    createApp testConf m1 m2 g
  )
  (describe
    "Web service"
    (do
      it
        "The 200 status code for a request GET /all-games"
        (do
          get "/all-games" `shouldRespondWith` 200
        )
      it
        "Initially an empty list for a request GET /all-games"
        (do
          get "/all-games" `shouldRespondWith` "[]"
        )
      it
        "A new game in response to a request POST /new"
        (do
          let name     = MkPlayerName "John"
              gameId   = MkGameId "nlrkwldbji"
              token    = MkPlayerToken "ipiwkzeatpadlgmnqedhiyxleqzmvl"
              halfGame = MkHalfInitialisedGame gameId (MkPlayer name token)
          postJSON "/new" (encode name) `shouldRespondWith` ResponseMatcher
            { matchHeaders = []
            , matchBody    = fromString . unpack . encode $ halfGame
            , matchStatus  = 200
            }
        )
      it
        "A non-empty list in response to a request GET /all-games"
        (do
          let name           = MkPlayerName "Jon"
              nameSerialised = encode name
              gameId         = MkGameId "nlrkwldbji"
              openGame       = MkOpenGameInfo gameId name
          postJSON "/new" nameSerialised
            >>                  get "/all-games"
            `shouldRespondWith` ResponseMatcher
                                  { matchHeaders = []
                                  , matchBody    = fromString
                                                   . unpack
                                                   . encode
                                                   $ [openGame]
                                  , matchStatus  = 200
                                  }
        )
    )
  )
