-- | The entry test module.
module Main
  ( main
  ) where

import           Test.Hspec                     ( describe
                                                , hspec
                                                )

import qualified LogicSpec                     as L
import qualified LogicInterfaceSpec            as LI
import qualified WebAPISpec                    as WA

main :: IO ()
main = hspec $ describe "All tests combined" $ do
  L.logicSpec
  LI.newGameSpec
  LI.allOpenGamesSpec
  LI.finaliseNewGameSpec
  LI.discInsertionSpec
  LI.gameStatusSpec
  WA.webAPISpec
