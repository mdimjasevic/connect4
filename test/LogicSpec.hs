module LogicSpec
  ( specColumn
  , specGrid
  , logicSpec
  ) where

import           Test.Hspec                     ( describe
                                                , it
                                                , shouldBe
                                                , Spec
                                                )

import           Logic                          ( Disc(EmptyDisc, Red, Blue)
                                                , discSequence
                                                , discSequenceBidirectional
                                                , down
                                                , downRight
                                                , emptyColumn
                                                , hasWon
                                                , insertIntoColumn
                                                , insertIntoGrid
                                                , isColumnFull
                                                , isGridFull
                                                , left
                                                , right
                                                , up
                                                , upLeft
                                                , upRight
                                                )
import           Logic.TestValues               ( g1
                                                , nultiColumnPun
                                                , g2
                                                , g3
                                                , fullGrid
                                                , c1
                                                , c2
                                                )


specColumn :: Spec
specColumn = describe "Column" $ do
  it "Column fullness" $ do
    isColumnFull emptyColumn `shouldBe` False
    isColumnFull c1 `shouldBe` False
    isColumnFull c2 `shouldBe` True
  it "Disc insertion" $ do
    insertIntoColumn Red emptyColumn
      `shouldBe` [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Red]
    insertIntoColumn Blue c1
      `shouldBe` [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Blue, Red]
    insertIntoColumn Red c1
      `shouldBe` [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Red, Red]
    insertIntoColumn Red c2 `shouldBe` c2

specGrid :: Spec
specGrid = describe "Grid" $ do
  it "Disc insertion" $ do
    insertIntoGrid Red 0 nultiColumnPun `shouldBe` nultiColumnPun
    insertIntoGrid Blue 0 g1 `shouldBe` insertIntoColumn Blue c1 : drop 1 g1
  it "Grid fullness" $ do
    isGridFull g1 `shouldBe` False
    isGridFull g2 `shouldBe` False
    isGridFull fullGrid `shouldBe` True
  it "Disc sequences" $ do
    discSequence 0 g1 up `shouldBe` [Red, EmptyDisc, EmptyDisc, EmptyDisc]
    discSequence 1 g1 down `shouldBe` [Blue, Blue, Blue]
    discSequence 2 g1 down `shouldBe` []
    discSequence 3 g1 down `shouldBe` [Red, Blue, Red, Blue]
    discSequence 6 g1 upLeft `shouldBe` [Blue, Blue, EmptyDisc, EmptyDisc]
    discSequence 4 g1 left `shouldBe` [Blue, Blue, EmptyDisc, Blue]
    discSequence 4 g1 right `shouldBe` [Blue, Blue, EmptyDisc]
    discSequence 3 g2 upRight `shouldBe` [Blue, EmptyDisc, EmptyDisc]
    discSequenceBidirectional 3 g1 up
      `shouldBe` [EmptyDisc, EmptyDisc, Red, Blue, Red, Blue]
    discSequenceBidirectional 3 g1 down
      `shouldBe` [Blue, Red, Blue, Red, EmptyDisc, EmptyDisc]
    discSequenceBidirectional 2 fullGrid downRight
      `shouldBe` [Blue, Red, Blue, Blue]
    discSequenceBidirectional 4 g2 left
      `shouldBe` [Blue, Blue, Red, Blue, EmptyDisc, EmptyDisc]
  it "Winner sequence searching" $ do
    hasWon Red g1 3 `shouldBe` False
    hasWon Red g3 1 `shouldBe` True
    hasWon Red g3 2 `shouldBe` True
    hasWon Red g3 3 `shouldBe` True
    hasWon Red g3 4 `shouldBe` True
    hasWon Blue fullGrid 3 `shouldBe` True
    hasWon Blue fullGrid 0 `shouldBe` False

logicSpec :: Spec
logicSpec = describe "Game Logic" $ do
  specColumn
  specGrid
