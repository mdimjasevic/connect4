module LogicInterfaceSpec where

import           Data.Function                  ( (&) )
import           Data.IORef                     ( IORef
                                                , newIORef
                                                , readIORef
                                                )
import           Data.Map.Strict                ( Map )
import qualified Data.Map.Strict               as M
import           Polysemy
import           Polysemy.Error
import           Polysemy.Input
import           Polysemy.AtomicState
import qualified System.Random                 as R
import           Test.Hspec                     ( describe
                                                , it
                                                , shouldBe
                                                , Spec
                                                )

import           InterfaceImplementation.InMemoryKVS
                                                ( runKVSInMemory )
import           Config
import           Logic                          ( emptyGrid
                                                , insertIntoColumn
                                                , Disc(Red, Blue)
                                                , ColumnIndex(..)
                                                )
import           Logic.TestValues               ( g1
                                                , c1
                                                )
import           Polysemy.Random                ( Random
                                                , runRandom
                                                )
import           KVS
import           LogicInterface
import           LogicInterface.Types


runNewGameInMemory
  :: IORef (Map GameId Player)
  -> IORef R.StdGen -- ^ random value generator
  -> Sem
       '[ Input Config
        , KVS GameId Player
        , AtomicState (Map GameId Player)
        , Error NewGameError
        , Random
        , Embed IO
        ]
       a
  -> IO (Either NewGameError a)
runNewGameInMemory m s program =
  program
    & runInputConst conf       -- run loading a fixed application configuration
    & runKVSInMemory m         -- run a key-value store as a map
    & runError @NewGameError   -- run error handling
    & runRandom s              -- run the random identifier generator
    & runM                     -- run the 'Sem' monad in the only remaining effect, i.e., IO
 where
  conf :: Config
  conf = MkConfig 2

runNewGame
  :: IORef (Map GameId Player)
  -> PlayerName
  -> IORef R.StdGen -- ^ random value generator
  -> IO (Either NewGameError HalfInitialisedGame)
runNewGame m player s = runNewGameInMemory m s (newGame player)

newGameSpec :: Spec
newGameSpec = describe
  "Initialising a new game"
  (do
    it
      "Empty player name throws an error"
      (do
        m <- newIORef M.empty
        g <- newIORef (R.mkStdGen 0)
        r <- runNewGame m (MkPlayerName "") g
        r `shouldBe` Left NewEmptyPlayerName
      )
    it
      "Successfully opens a new game"
      (do
        m <- newIORef M.empty
        g <- newIORef (R.mkStdGen 0)
        let name   = MkPlayerName "John"
            token  = MkPlayerToken "ipiwkzeatpadlgmnqedhiyxleqzmvl"
            gameId = MkGameId "nlrkwldbji"
        r <- runNewGame m name g
        r `shouldBe` Right (MkHalfInitialisedGame gameId (MkPlayer name token))
      )
    it
      "Successfully opens a new game in presence of other open games"
      (do
        let name1   = MkPlayerName "John"
            tok1    = MkPlayerToken "idjfsdqscmafhg"
            player1 = MkPlayer name1 tok1
            name2   = MkPlayerName "Jane"
            tok2    = MkPlayerToken "ipiwkzeatpadlgmnqedhiyxleqzmvl"
            player2 = MkPlayer name2 tok2
            gameId1 = MkGameId "apoidasqbn"
            gameId2 = MkGameId "nlrkwldbji"
        m <- newIORef (M.singleton gameId1 player1)
        g <- newIORef (R.mkStdGen 0)
        r <- runNewGame m name2 g
        r `shouldBe` Right (MkHalfInitialisedGame gameId2 player2)
      )
    it
      "Fails opening a new game when too many games are already open"
      (do
        let name1   = MkPlayerName "John"
            tok1    = MkPlayerToken "a"
            player1 = MkPlayer name1 tok1
            name2   = MkPlayerName "Jane"
            tok2    = MkPlayerToken "b"
            player2 = MkPlayer name2 tok2
            name3   = MkPlayerName "George"
            gameId1 = MkGameId "lkjdf;lkjdf"
            gameId2 = MkGameId "rjsdfjasdkjasd"
        openGames <- newIORef
          (M.fromList [(gameId1, player1), (gameId2, player2)])
        g <- newIORef (R.mkStdGen 0)
        r <- runNewGame openGames name3 g
        r `shouldBe` Left (NewTooManyOpenGames 2)
      )
  )

runAllOpenGamesInMemory
  :: IORef (Map GameId Player)
  -> Sem '[KVS GameId Player , AtomicState (Map GameId Player) , Embed IO] a
  -> IO a
runAllOpenGamesInMemory m program = program & runKVSInMemory m & runM

allOpenGamesSpec :: Spec
allOpenGamesSpec = describe
  "List all open games"
  (do
    it
      "Successfully returns an empty list"
      (do
        m <- newIORef M.empty
        r <- runAllOpenGamesInMemory m allOpenGames
        r `shouldBe` []
      )
    it
      "Sucessfully returns an existing list"
      (do
        let name1   = MkPlayerName "John"
            tok1    = MkPlayerToken "idjfsdqscmafhg"
            player1 = MkPlayer name1 tok1
            name2   = MkPlayerName "Jane"
            tok2    = MkPlayerToken "qiusdnaiythgz"
            player2 = MkPlayer name2 tok2
            gameId1 = MkGameId "lkjdf;lkjdf"
            gameId2 = MkGameId "rjsdfjasdkjasd"
            info1   = MkOpenGameInfo gameId1 name1
            info2   = MkOpenGameInfo gameId2 name2
        m <- newIORef (M.fromList [(gameId1, player1), (gameId2, player2)])
        r <- runAllOpenGamesInMemory m allOpenGames
        r `shouldBe` [info1, info2]
      )
  )

runFinaliseNewGameInMemory
  :: IORef (Map GameId Player)
  -> IORef (Map GameId GameStatus)
  -> IORef R.StdGen -- ^ random value generator
  -> Sem
       '[ KVS GameId Player
        , AtomicState (Map GameId Player)
        , KVS GameId GameStatus
        , AtomicState (Map GameId GameStatus)
        , Error FinaliseNewGameError
        , Random
        , Embed IO
        ]
       a
  -> IO (Either FinaliseNewGameError a)
runFinaliseNewGameInMemory mapOpen mapInProgress s program =
  program
    & runKVSInMemory mapOpen
    & runKVSInMemory mapInProgress
    & runError @FinaliseNewGameError
    & runRandom s
    & runM

runFinaliseNewGame
  :: IORef (Map GameId Player)
  -> IORef (Map GameId GameStatus)
  -> PlayerName
  -> GameId
  -> IORef R.StdGen -- ^ random value generator
  -> IO (Either FinaliseNewGameError PlayerToken)
runFinaliseNewGame mapOpen mapInProgress player game s =
  runFinaliseNewGameInMemory mapOpen
                             mapInProgress
                             s
                             (finaliseNewGame player game)

finaliseNewGameSpec :: Spec
finaliseNewGameSpec = describe
  "Finalising a new game"
  (do
    it
      "Empty player name throws an error"
      (do
        m1 <- newIORef M.empty
        m2 <- newIORef M.empty
        g  <- newIORef (R.mkStdGen 0)
        r <- runFinaliseNewGame m1 m2 (MkPlayerName "") (MkGameId "qwelksdfo") g
        r `shouldBe` Left FinaliseEmptyPlayerName
      )
    it
      "Non-existing game throws an error"
      (do
        let game = MkGameId "qwelksdfo"
        m1 <- newIORef M.empty
        m2 <- newIORef M.empty
        g  <- newIORef (R.mkStdGen 0)
        r  <- runFinaliseNewGame m1 m2 (MkPlayerName "John") game g
        r `shouldBe` Left (FinaliseUnknownGame game)
      )
    it
      "Successfully finishes initialising a game"
      (do
        let nameFirst       = MkPlayerName "John"
            tokenFirst      = MkPlayerToken "akldfadsfjkdvqp"
            first           = MkPlayer nameFirst tokenFirst

            nameSecond      = MkPlayerName "Jane"
            tokenSecond     = MkPlayerToken "nlrkwldbjiroiuubqpnceqcnspixno"
            second          = MkPlayer nameSecond tokenSecond

            gameId          = MkGameId "qwelksdfo"
            initialisedGame = MkInProgressGameState
              { inProgressGrid         = emptyGrid
              , inProgressOnMove       = SecondPlayer
              , inProgressFirstPlayer  = first
              , inProgressSecondPlayer = second
              }
        m1      <- newIORef (M.singleton gameId first)
        m2      <- newIORef M.empty
        g       <- newIORef (R.mkStdGen 0)
        r       <- runFinaliseNewGame m1 m2 nameSecond gameId g

        m1After <- readIORef m1
        m2After <- readIORef m2

        m1After `shouldBe` M.empty
        m2After `shouldBe` M.singleton gameId (InProgress initialisedGame)
        r `shouldBe` Right tokenSecond
      )
  )

runDiscInsertion
  :: IORef (Map GameId GameStatus)
  -> Sem
       '[ KVS GameId GameStatus
        , AtomicState (Map GameId GameStatus)
        , Error DiscInsertionError
        , Embed IO
        ]
       a
  -> IO (Either DiscInsertionError a)
runDiscInsertion m program =
  program & runKVSInMemory m & runError @DiscInsertionError & runM

discInsertionSpec :: Spec
discInsertionSpec = describe
  "Disc insertion"
  (do
    let name1          = MkPlayerName "John"
        tok1           = MkPlayerToken "asdfmoiqwyax"
        player1        = MkPlayer name1 tok1

        name2          = MkPlayerName "Jane"
        tok2           = MkPlayerToken "ipoiqwmasdjxiaqhg"
        player2        = MkPlayer name2 tok2

        gameId         = MkGameId "oierwhgmnasd"
        gameInProgress = MkInProgressGameState
          { inProgressGrid         = emptyGrid
          , inProgressOnMove       = FirstPlayer
          , inProgressFirstPlayer  = player1
          , inProgressSecondPlayer = player2
          }
        gameSt = InProgress gameInProgress

    it
      "Fails to insert into a non-existing column"
      (do
        let col       = MkColumnIndex 10
            insertion = MkDiscInsertion col tok1

        m <- newIORef (M.singleton gameId (Draw emptyGrid))
        r <- runDiscInsertion m (insertDisc insertion)
        r `shouldBe` Left (InsertNonExistingColumn col)
      )
    it
      "Fails to insert via an invalid token"
      (do
        let badToken  = MkPlayerToken "a"
            insertion = MkDiscInsertion (MkColumnIndex 0) badToken
            gameState = InProgress
              (MkInProgressGameState { inProgressGrid         = emptyGrid
                                     , inProgressOnMove       = FirstPlayer
                                     , inProgressFirstPlayer  = player1
                                     , inProgressSecondPlayer = player2
                                     }
              )
        m <- newIORef (M.singleton gameId gameState)
        r <- runDiscInsertion m (insertDisc insertion)
        r `shouldBe` Left (InsertInvalidToken badToken)
      )
    it
      "Fails to insert because not on move"
      (do
        let insertion = MkDiscInsertion (MkColumnIndex 0) tok2
        m <- newIORef (M.singleton gameId gameSt)
        r <- runDiscInsertion m (insertDisc insertion)
        r `shouldBe` Left (InsertNotOnMove SecondPlayer FirstPlayer)
      )
    it
      "Fails to insert into a full column"
      (do
        let col       = MkColumnIndex 5
            insertion = MkDiscInsertion col tok1
            gameSt'   = InProgress gameInProgress { inProgressGrid = g1 }
        m <- newIORef (M.singleton gameId gameSt')
        r <- runDiscInsertion m (insertDisc insertion)
        r `shouldBe` Left (InsertFullColumn col)
      )
    it
      "Succeeds in inserting a disc"
      (do
        let insertion1    = MkDiscInsertion (MkColumnIndex 0) tok1
            gameStBefore1 = InProgress gameInProgress { inProgressGrid = g1 }
            gameStAfter1  = InProgress gameInProgress
              { inProgressGrid   = insertIntoColumn Red c1 : drop 1 g1
              , inProgressOnMove = nextToPlay (inProgressOnMove gameInProgress)
              }
        m1Before <- newIORef (M.singleton gameId gameStBefore1)
        r        <- runDiscInsertion m1Before (insertDisc insertion1)
        m1After  <- readIORef m1Before

        m1After `shouldBe` M.singleton gameId gameStAfter1
        r `shouldBe` Right gameStAfter1

        let insertion2    = MkDiscInsertion (MkColumnIndex 0) tok2
            gameStBefore2 = InProgress gameInProgress
              { inProgressGrid   = g1
              , inProgressOnMove = SecondPlayer
              }
            gameStAfter2 = InProgress gameInProgress
              { inProgressGrid = insertIntoColumn Blue c1 : drop 1 g1
              }
        m2Before <- newIORef (M.singleton gameId gameStBefore2)
        r'       <- runDiscInsertion m2Before (insertDisc insertion2)
        m2After  <- readIORef m2Before

        m2After `shouldBe` M.singleton gameId gameStAfter2
        r' `shouldBe` Right gameStAfter2
      )
  )

runGameStatus
  :: IORef (Map GameId GameStatus)
  -> Sem
       '[ KVS GameId GameStatus
        , AtomicState (Map GameId GameStatus)
        , Error GameStatusError
        , Embed IO
        ]
       a
  -> IO (Either GameStatusError a)
runGameStatus m program =
  program & runKVSInMemory m & runError @GameStatusError & runM

gameStatusSpec :: Spec
gameStatusSpec = describe
  "Game status"
  (do
    let status = Victory FirstPlayer g1
        gameId = MkGameId "a"

    it
      "Fails with an error for an invalid game identifier"
      (do
        m <- newIORef M.empty
        r <- runGameStatus m (gameStatus gameId)
        r `shouldBe` Left (StatusNonExistingGame gameId)

        let badGameId = MkGameId "b"
        m' <- newIORef (M.singleton gameId status)
        r' <- runGameStatus m' (gameStatus badGameId)
        r' `shouldBe` Left (StatusNonExistingGame badGameId)
      )
    it
      "Successfully returns an existing game's state"
      (do
        m <- newIORef (M.singleton gameId status)
        r <- runGameStatus m (gameStatus gameId)
        r `shouldBe` Right status
      )
  )
