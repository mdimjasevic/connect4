# Connect4

This is a connection board game implemented in Haskell.

## Running, Testing and Documentation

To run it locally on port 8080, run:

    stack run

To run tests, execute:

    stack test

To generate documentation from the annotated source code:

    stack haddock

To generate web API documentation, run:

    stack run

and then kill the process. This will generate API documentation in the `docs`
directory.

## Technology

There are lots of Haskell and programming language theory features that this
code base makes use of, to name a few:

- An eDSL for describing a web API, as provided by the Servant framework (see
  `src/WebAPI.hs`). Servant uses monad transformers for its central type
  family `ServerT` and for the `Handler` monad,
- An effect system as provided by the Polysemy library. The game uses
  several eDSLs for these effects: input, error handling, state
  updating in a concurrent application, random value generator and a
  key value store. This effect system is plugged in to Servant's monad
  transformers, which provides separation of domain logic from web server
  concerns.
- The code is structured in layers, where a layer has access only to
  functionality provided by itself or by lower-level layers. This
  allows for ease of testing and maintenance, and swapping a dependency
  library,
- Function totality,
- Generalised algebraic data types,
- Type differentiation via the `newtype` declaration. This has already
  brought to light a few bugs during development.

## To do

There are several aspects where the implementation could be improved:

- The implementation currently features no frontend client. This is to be
  implemented in Haskell.
- The `Logic` module implements a dummy empty disc to signal for absence
  of a disc in a column position. This is to be redone such that there are
  only two disc colours and thereby make illegal states not representable.
- More tests are needed for the web API. It is not clear how to thread the
  state from one HTTP request to another.
- Fix-sized vectors should be used instead of unbounded lists in the game
  logic (the `Logic` module). This will reflect the constraints on the grid
  size in the code.
