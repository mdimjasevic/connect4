module Main where

import           Data.IORef                     ( newIORef )
import qualified Data.Map.Strict               as M
import           Network.Wai.Handler.Warp       ( run )
import qualified System.Random                 as R

import           InterfaceImplementation.App    ( createApp )
import           Config
import           Servant.Docs                   ( markdown
                                                , docsWithIntros
                                                , DocIntro(..)
                                                )
import           WebAPI                         ( api )


main :: IO ()
main = do
  -- generate documentation
  let intro = DocIntro
        "Connect4 game API documentation"
        [ "This is basic API documentation for Connect4. "
            ++ "The implementation of the API is in `src/WebInterface.hs`."
        ]
      doc = markdown (docsWithIntros [intro] api)
  writeFile "docs/API-documentation.md" doc
  -- From API-documentation.md it is possible to generate HTML via pandoc
  -- (if installed). First fetch an HTML template:
  --
  --   curl https://raw.githubusercontent.com/ryangrose/easy-pandoc-templates/master/html/bootstrap_menu.html -o bootstrap_menu.html
  --
  -- Then generate HTML from Markdown:
  --
  --   pandoc API-documentation.md -f markdown -o API-documentation.html --template bootstrap_menu.html --toc

  let conf = MkConfig 10
      port = 8080 :: Int
  playerMap <- newIORef M.empty
  statusMap <- newIORef M.empty
  gen       <- newIORef (R.mkStdGen 1)
  app       <- createApp conf playerMap statusMap gen
  putStrLn ("Starting a server on port " ++ show port)
  run port app
