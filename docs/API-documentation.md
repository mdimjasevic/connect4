## Connect4 game API documentation

This is basic API documentation for Connect4. The implementation of the API is in `src/WebInterface.hs`.

## GET /all-games

### Fetch information on all open games.


### Response:

- Status code 200
- Headers: []

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
[]
```

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
[{"openGameInfoGameId":"apoidasqbn","openGameInfoFirstPlayer":"John"}]
```

- Example (`application/json;charset=utf-8`):

```javascript
[{"openGameInfoGameId":"apoidasqbn","openGameInfoFirstPlayer":"John"},{"openGameInfoGameId":"apoidasqbn","openGameInfoFirstPlayer":"John"}]
```

## GET /game/:game-id

### Fetch a game's status.


### Captures:

- *game-id*: game identifier

### Response:

- Status code 200
- Headers: []

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- When the game is in progress (`application/json;charset=utf-8`, `application/json`):

```javascript
{"status":"in-progress","second-player":"John","first-player":"Jane","on-move":"first","grid":[["-","-","-","-","-","r"],["-","-","-","b","b","b"],["-","-","-","-","-","-"],["-","-","r","b","r","b"],["-","-","-","b","b","r"],["r","b","b","b","r","b"],["-","-","-","-","b","r"]]}
```

- When the grid is full and it is a draw (`application/json;charset=utf-8`, `application/json`):

```javascript
{"status":"draw","grid":[["b","r","r","b","r","b"],["r","r","b","b","r","r"],["b","r","r","b","r","b"],["b","b","b","r","b","r"],["r","r","r","b","b","b"],["r","r","b","b","r","r"],["b","b","r","b","r","b"]]}
```

- When there is a winner (`application/json;charset=utf-8`):

```javascript
{"status":"victory","grid":[["b","r","r","b","r","b"],["r","r","b","b","r","r"],["b","b","r","b","r","b"],["b","b","b","r","b","r"],["r","r","r","b","b","b"],["r","r","b","b","r","r"],["b","b","r","b","r","b"]],"winner":"first"}
```

## POST /game/:game-id/finalise

### Finalise an open game by the second player joining.


### Captures:

- *game-id*: game identifier

### Request:

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
"John"
```

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
"Jane"
```

### Response:

- Status code 200
- Headers: []

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- When the player's name and game ID are valid (`application/json;charset=utf-8`, `application/json`):

```javascript
"ipiwkzeatpadlgmnqedhiyxleqzmvl"
```

- When the player's name and game ID are valid (`application/json;charset=utf-8`, `application/json`):

```javascript
"dlgmnqedhiyxleqzmvlipiwkzeatpa"
```

## POST /insert-disc

### Insert a disc into a grid of an in-progress game.


### Request:

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
{"discInsertionColumn":0,"discInsertionPlayerToken":"ipiwkzeatpadlgmnqedhiyxleqzmvl"}
```

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
{"discInsertionColumn":5,"discInsertionPlayerToken":"dlgmnqedhiyxleqzmvlipiwkzeatpa"}
```

### Response:

- Status code 200
- Headers: []

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- When the game is in progress (`application/json;charset=utf-8`, `application/json`):

```javascript
{"status":"in-progress","second-player":"John","first-player":"Jane","on-move":"first","grid":[["-","-","-","-","-","r"],["-","-","-","b","b","b"],["-","-","-","-","-","-"],["-","-","r","b","r","b"],["-","-","-","b","b","r"],["r","b","b","b","r","b"],["-","-","-","-","b","r"]]}
```

- When the grid is full and it is a draw (`application/json;charset=utf-8`, `application/json`):

```javascript
{"status":"draw","grid":[["b","r","r","b","r","b"],["r","r","b","b","r","r"],["b","r","r","b","r","b"],["b","b","b","r","b","r"],["r","r","r","b","b","b"],["r","r","b","b","r","r"],["b","b","r","b","r","b"]]}
```

- When there is a winner (`application/json;charset=utf-8`):

```javascript
{"status":"victory","grid":[["b","r","r","b","r","b"],["r","r","b","b","r","r"],["b","b","r","b","r","b"],["b","b","b","r","b","r"],["r","r","r","b","b","b"],["r","r","b","b","r","r"],["b","b","r","b","r","b"]],"winner":"first"}
```

## POST /new

### Opens a new game.


### Request:

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
"John"
```

- Example (`application/json;charset=utf-8`, `application/json`):

```javascript
"Jane"
```

### Response:

- Status code 200
- Headers: []

- Supported content types are:

    - `application/json;charset=utf-8`
    - `application/json`

- When the player name is available (`application/json;charset=utf-8`, `application/json`):

```javascript
{"halfInitialisedGamePlayer":{"playerName":"Jane","playerToken":"ipiwkzeatpadlgmnqedhiyxleqzmvl"},"halfInitialisedGameGameId":"nlrkwldbji"}
```

