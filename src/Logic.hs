module Logic
  ( Disc(..)
  , Column
  , emptyColumn
  , isColumnFull
  , insertIntoColumn
  , ColumnIndex(..)
  , Grid
  , emptyGrid
  , isGridFull
  , insertIntoGrid
  , discSequence
  , discSequenceBidirectional
  , up
  , upRight
  , right
  , downRight
  , down
  , downLeft
  , left
  , upLeft
  , hasWon
  ) where

import           Data.Aeson                     ( FromJSON(parseJSON)
                                                , ToJSON(toJSON)
                                                , Value(Number, String)
                                                )
import           Data.List                      ( tails )
import           Data.Maybe                     ( isJust
                                                , catMaybes
                                                , listToMaybe
                                                )
import           Data.Scientific                ( Scientific(coefficient) )
import           Numeric.Natural                ( Natural )


-- | A data type for discs. It is a two-player game, hence there are two
-- colors a disc can be in.
data Disc
  = Red
  | Blue
  | EmptyDisc -- TODO(md): Get rid of this data constructor
  deriving (Eq, Ord, Show)

instance ToJSON Disc where
  toJSON Red       = String "r"
  toJSON Blue      = String "b"
  toJSON EmptyDisc = String "-"

-- | A column on the grid is represented as a list of discs. By agreement,
-- a column can have at most 6 discs (the least is 0 and the column is empty
-- in such a case).
type Column = [Disc]

-- | An empty 'Column'.
emptyColumn :: Column
emptyColumn =
  [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc]


-- | The game grid consists of a list of 'Column's. By agreement, a grid
-- is made of exactly 7 'Column's.
type Grid = [Column]

-- | An empty 'Grid', which is a value of 7 empty 'Column's.
emptyGrid :: Grid
emptyGrid =
  [ emptyColumn
  , emptyColumn
  , emptyColumn
  , emptyColumn
  , emptyColumn
  , emptyColumn
  , emptyColumn
  ]

-- | The function checks if a 'Column' is full, i.e., if there are exactly
-- six discs in it.
isColumnFull :: Column -> Bool
isColumnFull []      = False
isColumnFull (h : _) = h /= EmptyDisc

-- | The function inserts a 'Disc' into a 'Column'. If the column is full,
-- the column is returned intact. Otherwise, the disc is added to the top
-- of the column.
insertIntoColumn :: Disc -> Column -> Column
insertIntoColumn z s = if isColumnFull s
  then s
  else
    let (empptyPart, fullPart) = span (== EmptyDisc) s
    in  drop 1 empptyPart ++ [z] ++ fullPart

-- | A column index wrapped in a newtype. By using this type in 'InsertDisc',
-- it is guaranteed that the value is never less than 0 or equal to or greater
-- than 7 (see the implementation of the 'FromJSON' instance for
-- 'ColumnIndex').
newtype ColumnIndex = MkColumnIndex { getColumnIndex :: Natural }
  deriving newtype (Eq, Num, Ord, Read, Show, ToJSON)

instance FromJSON ColumnIndex where
  parseJSON (Number s)
    | s < 0     = fail (show s ++ " is not zero or a positive number!")
    | s < 7     = pure . MkColumnIndex . fromIntegral . coefficient $ s
    | otherwise = fail (show s ++ " is not less than 7")
  parseJSON _ = fail "Invalid JSON for a column index"

-- | The function isnerts a 'Disc' into a 'Grid'. If attempting to insert into
-- a full column, the grid will stay intact.
insertIntoGrid
  :: Disc        -- ^ The disc to be inserted
  -> ColumnIndex -- ^ The column index to insert into (0 is the leftmost and 6 is
                 -- the rightmost)
  -> Grid        -- ^ The grid to insert into
  -> Grid
insertIntoGrid d index g = if index >= 7
  then g
  else
    let (toColumn, fromColumn) =
          splitAt (fromIntegral . getColumnIndex $ index) g
        col    = g !! (fromIntegral . getColumnIndex $ index)
        newCol = insertIntoColumn d col
    in  toColumn ++ [newCol] ++ drop 1 fromColumn

-- | The function checks if all the space is taken. It returns True if all the
-- space is taken, and False otherwise.
isGridFull :: Grid -> Bool
isGridFull = all isColumnFull

-- | The function checks if a player with a given 'Disc' color has a victory
-- at the column top.
hasWonAtColumnTop :: Disc -> Column -> Bool
hasWonAtColumnTop d c =
  let (stretch, _ostalo) = span (== d) c in length stretch >= 4

-- hasRedWonAtColumnTop :: Column -> Bool
-- hasRedWonAtColumnTop = hasWonAtColumnTop Red

-- hasBlueWonAtColumnTop :: Column -> Bool
-- hasBlueWonAtColumnTop = hasWonAtColumnTop Blue

-- | The function checks if a player determined by the 'Disc' has a victory
-- in the given 'Column'.
hasWonInColumn :: Disc -> Column -> Bool
hasWonInColumn d c =
  let allSufices = tails c in any (hasWonAtColumnTop d) allSufices

-- | The function gets a disck from the given row, where the 0 row is
-- at the grid top. If there is no disc at the position, the function
-- returns Nothing. Otherwise it returns the disc wrapped in a @Just@.
--
-- The function can also be used to select a grid column.
atPos :: Int -> [a] -> Maybe a
atPos index _ | index < 0 = Nothing
atPos index s             = listToMaybe (drop index s)


-- | The direction index for a relative move in the 'move' list. Valid values
-- rangr from 0 to 7 inclusive.
type Direction = Int

up, upRight, right, downRight, down, downLeft, left, upLeft :: Int
up = 0
upRight = 1
right = 2
downRight = 3
down = 4
downLeft = 5
left = 6
upLeft = 7

-- | A list of relative offsets in 8 possible directions. The first and second
-- coordinate is for a column and row, respectively.
offset :: [(Int, Int)]
offset =
  [ (0 , -1)  -- up
  , (1 , -1)  -- up-right
  , (1 , 0)   -- right
  , (1 , 1)   -- down-right
  , (0 , 1)   -- down
  , (-1, 1)  -- down-left
  , (-1, 0)  -- left
  , (-1, -1) -- up-left
  ]

-- | A number of steps to make for a relative move.
type NumOfSteps = Int

-- | The function computes new coordinates given the direction and number of
-- steps to make relative to the starting column and row.
moveInDirection :: Direction -> NumOfSteps -> Int -> Int -> (Int, Int)
moveInDirection dir numOfSteps colIndex rowIndex =
  ( colIndex + numOfSteps * fst (offset !! dir)
  , rowIndex + numOfSteps * snd (offset !! dir)
  )

-- | The function extracts at most 4 discs in the given direction, starting
-- at the column given by the index.
discSequence :: Int -> Grid -> Direction -> Column
discSequence colIndex g dir =
  let
    startCol = (g !! colIndex)
    rowIndex = length (takeWhile (== EmptyDisc) startCol)
  in
    if rowIndex >= 6
      then []
      else
        let
          moves =
            [ moveInDirection dir 1 colIndex rowIndex
            , moveInDirection dir 2 colIndex rowIndex
            , moveInDirection dir 3 colIndex rowIndex
            ]
          stupac1 = atPos (fst (moves !! 0)) g
          stupac2 = atPos (fst (moves !! 1)) g
          stupac3 = atPos (fst (moves !! 2)) g
          mDisc1  = stupac1 >>= atPos (snd (moves !! 0))
          mDisc2  = stupac2 >>= atPos (snd (moves !! 1))
          mDisc3  = stupac3 >>= atPos (snd (moves !! 2))
          nizMogucihDisca =
            [Just ((g !! colIndex) !! rowIndex), mDisc1, mDisc2, mDisc3]
          stvarniNiz = catMaybes (takeWhile isJust nizMogucihDisca)
        in
          stvarniNiz

-- | The function extracts at most 4 discs in the given direction, starting
-- at the column given by the index, and expands the sequence with at most
-- 4 discs from the opposite direction.
discSequenceBidirectional :: Int -> Grid -> Direction -> Column
discSequenceBidirectional colIndex p smjer =
  let seq1              = discSequence colIndex p smjer
      oppositeDirection = (smjer + 4) `mod` 8
      seq2              = discSequence colIndex p oppositeDirection
  in  reverse (drop 1 seq1) ++ seq2

-- | The function checks if a player with a 'Disc' color has a victory
-- by inserting a disc into a column given by the column index. If they
-- have a victory, the function returns @True@, and returns @False@ otherwise.
hasWon :: Disc -> Grid -> Int -> Bool
hasWon d g colIndex =
  let dirs = [0, 1, 2, 3]
      seqs = fmap (discSequenceBidirectional colIndex g) dirs
  in  any (hasWonInColumn d) seqs
