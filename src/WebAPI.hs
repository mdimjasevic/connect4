module WebAPI where

import           Polysemy                       ( Members
                                                , Sem
                                                )
import           Polysemy.Error                 ( Error )
import           Polysemy.Input                 ( Input )
import           Servant                        ( type (:<|>)(..)
                                                , Capture
                                                , JSON
                                                , Proxy(..)
                                                , ReqBody
                                                , type (:>)
                                                , Get
                                                , Post
                                                , Summary
                                                )
import           Servant.Server                 ( ServerT )

import           Config                         ( Config )
import           Polysemy.Random                ( Random )
import           KVS                            ( KVS )
import qualified LogicInterface                as LI
import           LogicInterface.Types           ( GameId(..)
                                                , PlayerToken(..)
                                                , HalfInitialisedGame(..)
                                                , Player(..)
                                                , PlayerName(..)
                                                , OpenGameInfo(..)
                                                , GameStatus(..)
                                                , DiscInsertion
                                                )


-- | A type laying out the endpoint for GET \/all-games. It serves information
-- on all open games.
type AllOpenGames =
     Summary "Fetch information on all open games."
  :> "all-games"
  :> Get '[JSON] [OpenGameInfo]

-- | A type laying out the endpoint for POST \/new. In the POST request body it
-- is necessary to provide the first player's name. The response contains an
-- ID of the newly opened game as well as the first player's secret token.
type NewGame =
     Summary "Opens a new game."
  :> "new"
  :> ReqBody '[JSON] PlayerName
  :> Post '[JSON] HalfInitialisedGame

-- | A type laying out the endpoint for POST \/game\/:game-id\/finalise. The
-- POST request should contain the second player's name. The response is
-- made of the second player's secret token. Unlike for the POST \/new endpoint,
-- the response contains no game ID: from the request path it follows the
-- second player already knows it. IDs of all the open games are available
-- via the GET \/all-games endpoint.
type FinaliseNewGame =
     Summary "Finalise an open game by the second player joining."
  :> "game"
  :> ReqBody '[JSON] PlayerName
  :> Capture "game-id" GameId
  :> "finalise"
  :> Post '[JSON] PlayerToken

-- | A type laying out the endpoint for POST \/insert-disc. The request
-- should contains a column index and a secret token that is inserting
-- a disc. The response is a game status for a valid disc insertion.
type InsertDisc =
     Summary "Insert a disc into a grid of an in-progress game."
  :> "insert-disc"
  :> ReqBody '[JSON] DiscInsertion
  :> Post '[JSON] GameStatus

-- | A type laying out the endpoint for GET \/game\/:game-id. The request body
-- is empty. The response is according to the 'GameStatus' data type.
type GameState =
     Summary "Fetch a game's status."
  :> "game"
  :> Capture "game-id" GameId
  :> Get '[JSON] GameStatus

-- | A type laying out all the endpoints, i.e., the game API.
type GameAPI =
       AllOpenGames
  :<|> NewGame
  :<|> FinaliseNewGame
  :<|> InsertDisc
  :<|> GameState

-- | A value whose sole purpose is to help GHC with type inference. It is used
-- in the 'InterfaceImplementation.App.createApp' and
-- 'InterfaceImplementation.App.liftServer' functions.
api :: Proxy GameAPI
api = Proxy

-- | A server for the 'GameAPI' endpoints. For each endpoint there is a
-- corresponding function from the logic interface (the LI qualified
-- module import).
gameServer
  :: ( Members
         '[ Input Config
          , KVS GameId Player
          , KVS GameId GameStatus
          , Random
          , Error LI.NewGameError
          , Error LI.FinaliseNewGameError
          , Error LI.DiscInsertionError
          , Error LI.GameStatusError
          ]
         r
     )
  => ServerT GameAPI (Sem r)
gameServer =
       LI.allOpenGames
  :<|> LI.newGame
  :<|> LI.finaliseNewGame
  :<|> LI.insertDisc
  :<|> LI.gameStatus
