-- | A simple language for a key-value store
module KVS
  ( KVS(..)
  , listAllKVS
  , getKVS
  , insertKVS
  , deleteKVS
  , sizeKVS
  , setKVS
  ) where

import           Numeric.Natural                ( Natural )
import           Polysemy                       ( Member
                                                , Sem
                                                , makeSem
                                                )


-- | A parameterised key-value store defined via a GADT
data KVS k v m a where
  ListAllKVS ::KVS k v m [(k, v)]
  GetKVS     ::k -> KVS k v m (Maybe v)
  InsertKVS  ::k -> v -> KVS k v m ()
  DeleteKVS  ::k -> KVS k v m ()

makeSem ''KVS

-- | Computes the number of key-value pairs in the store.
sizeKVS :: Member (KVS k v) r => Sem r Natural
sizeKVS = fromIntegral . length <$> listAllKVS

-- | Sets a new value for the key. If a value existed for the key,
-- the function returns that old value wrapped in a @Just@, otherwise it
-- returns Nothing.
setKVS :: Member (KVS k v) r => k -> v -> Sem r (Maybe v)
setKVS key value = do
  mOldV <- getKVS key
  insertKVS key value
  pure mOldV
