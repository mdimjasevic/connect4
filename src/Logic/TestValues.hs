module Logic.TestValues where

import           Logic                          ( Disc(..)
                                                , Column
                                                , Grid
                                                , emptyColumn
                                                )


c1, c2 :: Column
c1 = [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Red]
c2 = [Red, Blue, Red, Blue, Red, Red]

nultiColumnPun :: Grid
nultiColumnPun =
  [ c2
  , emptyColumn
  , emptyColumn
  , emptyColumn
  , emptyColumn
  , emptyColumn
  , emptyColumn
  ]

g1 :: Grid
g1 =
  [ c1
  , [EmptyDisc, EmptyDisc, EmptyDisc, Blue, Blue, Blue]
  , emptyColumn
  , [EmptyDisc, EmptyDisc, Red, Blue, Red, Blue]
  , [EmptyDisc, EmptyDisc, EmptyDisc, Blue, Blue, Red]
  , [Red, Blue, Blue, Blue, Red, Blue]
  , [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Blue, Red]
  ]

g2 :: Grid
g2 =
  [ [Blue, Red, Red, Blue, Red, Blue]
  , [Red, Red, Blue, Blue, Red, Red]
  , [Blue, Blue, Red, Blue, Red, Blue]
  , [EmptyDisc, EmptyDisc, Blue, Red, Blue, Red]
  , [EmptyDisc, EmptyDisc, EmptyDisc, Blue, Blue, Blue]
  , emptyColumn
  , emptyColumn
  ]

g3 :: Grid
g3 =
  [ emptyColumn
  , [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Red]
  , [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Red]
  , [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Red]
  , [EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, EmptyDisc, Red]
  , emptyColumn
  , emptyColumn
  ]

-- | A full grid
fullGrid :: Grid
fullGrid =
  [ [Blue, Red, Red, Blue, Red, Blue]
  , [Red, Red, Blue, Blue, Red, Red]
  , [Blue, Blue, Red, Blue, Red, Blue]
  , [Blue, Blue, Blue, Red, Blue, Red]
  , [Red, Red, Red, Blue, Blue, Blue]
  , [Red, Red, Blue, Blue, Red, Red]
  , [Blue, Blue, Red, Blue, Red, Blue]
  ]

-- | A game that ended in a draw
drawGrid :: Grid
drawGrid =
  [ [Blue, Red, Red, Blue, Red, Blue]
  , [Red, Red, Blue, Blue, Red, Red]
  , [Blue, Red, Red, Blue, Red, Blue]
  , [Blue, Blue, Blue, Red, Blue, Red]
  , [Red, Red, Red, Blue, Blue, Blue]
  , [Red, Red, Blue, Blue, Red, Red]
  , [Blue, Blue, Red, Blue, Red, Blue]
  ]
