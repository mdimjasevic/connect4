{-# LANGUAGE FlexibleInstances #-}
module LogicInterface.Types where

import           Data.Aeson                     ( object
                                                , FromJSON
                                                , ToJSON(toJSON)
                                                )
import           Data.Aeson.Types               ( KeyValue((.=))
                                                , Value(String)
                                                )
import           Data.List.Extra                ( trim )
import           Data.Proxy                     ( Proxy )
import           Data.Text                      ( pack
                                                , Text
                                                )
import           GHC.Generics                   ( Generic )
import           Servant                        ( Capture
                                                , FromHttpApiData
                                                )
import qualified System.Random                 as R

import           Logic                          ( ColumnIndex(..)
                                                , Grid
                                                )
import           Logic.TestValues               ( drawGrid
                                                , g1
                                                , fullGrid
                                                )
import           Servant.Docs                   ( DocCapture(..)
                                                , ToCapture(..)
                                                , ToSample(..)
                                                )
import           Numeric.Natural                ( Natural )


-- | A game identifier wrapped in a new type.
newtype GameId = MkGameId { getGameId :: String }
  deriving newtype (Eq, FromHttpApiData, Ord, Show, ToJSON)

-- | The function generates a random character sequence of the given length
-- via the provided generator.
randomCharSequence
  :: R.RandomGen g
  => g    -- ^ Generator
  -> Word -- ^ The sequence length
  -> String
randomCharSequence gen l = take (fromIntegral l) (R.randomRs ('a', 'z') gen)

instance R.Random GameId where
  -- | Generates a random sequence of 10 characters.
  random :: R.RandomGen g => g -> (GameId, g)
  random gen = (MkGameId (randomCharSequence gen 10), snd . R.split $ gen)

  randomR :: R.RandomGen g => (GameId, GameId) -> g -> (GameId, g)
  randomR _ = R.random

instance ToCapture (Capture "game-id" GameId) where
  toCapture :: Proxy (Capture "game-id" GameId) -> DocCapture
  toCapture _ = DocCapture "game-id"         -- name
                           "game identifier" -- description

-- | A new type wrapper for a player name.
newtype PlayerName = MkPlayerName { getPlayerName :: String }
  deriving (Eq, Generic)
  deriving newtype (Show, ToJSON, FromJSON)

instance ToSample PlayerName where
  toSamples :: Proxy PlayerName -> [(Text, PlayerName)]
  toSamples _ = [("", MkPlayerName "John"), ("", MkPlayerName "Jane")]

isNameEmpty :: PlayerName -> Bool
isNameEmpty (MkPlayerName name) = trim name == ""

dropWhitespace :: PlayerName -> PlayerName
dropWhitespace (MkPlayerName name) = MkPlayerName (trim name)

-- | A type for an open game with data visible to everyone. It comprises
-- a game ID and a player name that opened the game.
data OpenGameInfo = MkOpenGameInfo
  { openGameInfoGameId      :: GameId
  , openGameInfoFirstPlayer :: PlayerName
  }
  deriving (Eq, Generic, Show)

instance ToJSON OpenGameInfo

instance ToSample OpenGameInfo where
  toSamples :: Proxy OpenGameInfo -> [(Text, OpenGameInfo)]
  toSamples _ =
    [("", mkGame "apoidasqbn" "John"), ("", mkGame "nlrkwldbji" "Jane")]
   where
    mkGame :: String -> String -> OpenGameInfo
    mkGame gameId name = MkOpenGameInfo (MkGameId gameId) (MkPlayerName name)

-- | A secret player token wrapped in a new type.
newtype PlayerToken = MkPlayerToken { getPlayerToken :: String }
  deriving (Eq, Generic, Show)
  deriving newtype (ToJSON, FromJSON)

instance R.Random PlayerToken where
  -- | Generates a random character sequence of length 30.
  random :: R.RandomGen g => g -> (PlayerToken, g)
  random gen =
    let i = MkPlayerToken (randomCharSequence gen 30)
    in  (i, snd . R.split $ gen)

  randomR
    :: R.RandomGen g => (PlayerToken, PlayerToken) -> g -> (PlayerToken, g)
  randomR _ = R.random

instance ToSample PlayerToken where
  toSamples :: Proxy PlayerToken -> [(Text, PlayerToken)]
  toSamples _ =
    [ ( "When the player's name and game ID are valid"
      , MkPlayerToken "ipiwkzeatpadlgmnqedhiyxleqzmvl"
      )
    , ( "When the player's name and game ID are valid"
      , MkPlayerToken "dlgmnqedhiyxleqzmvlipiwkzeatpa"
      )
    ]
-- | A type describing a player: its name and token.
data Player = MkPlayer
  { playerName  :: PlayerName
  , playerToken :: PlayerToken
  }
  deriving (Eq, Generic, Show)

instance ToJSON Player

-- | A data type for a new open game, made of a 'GameId' and a 'Player'.
data HalfInitialisedGame = MkHalfInitialisedGame
  { halfInitialisedGameGameId :: GameId
  , halfInitialisedGamePlayer :: Player
  }
  deriving (Eq, Generic, Show)

instance ToJSON HalfInitialisedGame

instance ToSample HalfInitialisedGame where
  toSamples :: Proxy HalfInitialisedGame -> [(Text, HalfInitialisedGame)]
  toSamples _ =
    [ ( "When the player name is available"
      , mkGame "nlrkwldbji" "Jane" "ipiwkzeatpadlgmnqedhiyxleqzmvl"
      )
    ]
   where
    mkGame :: String -> String -> String -> HalfInitialisedGame
    mkGame gameId name token = MkHalfInitialisedGame
      (MkGameId gameId)
      (MkPlayer (MkPlayerName name) (MkPlayerToken token))

-- | A data type describing a disc insertion. It comprises a column index
-- that is being inserted into and a secret player token. The color of the
-- disc is uniquely determined by the token: the player can be in one game
-- only at any given time, and two tokens are associated with each game.
-- The current state of the grid and game is also uniquely determined by
-- the token.
data DiscInsertion = MkDiscInsertion
  { discInsertionColumn      :: ColumnIndex
  , discInsertionPlayerToken :: PlayerToken
  }
  deriving Generic

instance ToJSON DiscInsertion
instance FromJSON DiscInsertion

instance ToSample DiscInsertion where
  toSamples :: Proxy DiscInsertion -> [(Text, DiscInsertion)]
  toSamples _ =
    [ ("", mkInsertion 0 "ipiwkzeatpadlgmnqedhiyxleqzmvl")
    , ("", mkInsertion 5 "dlgmnqedhiyxleqzmvlipiwkzeatpa")
    ]
   where
    mkInsertion :: Natural -> String -> DiscInsertion
    mkInsertion indeks token =
      MkDiscInsertion (MkColumnIndex indeks) (MkPlayerToken token)

-- | The function checks if a disc insertion is out of grid boundaries.
isOutOfGrid :: DiscInsertion -> Bool
isOutOfGrid = (> 6) . getColumnIndex . discInsertionColumn

-- | A data type indicating which player is considered, e.g., a tag.
-- There are only two possible outcomes.
data WhichPlayer = FirstPlayer | SecondPlayer deriving Eq

instance Show WhichPlayer where
  show FirstPlayer  = "first"
  show SecondPlayer = "second"

instance ToJSON WhichPlayer where
  toJSON = String . pack . show

instance R.Random WhichPlayer where
  -- | Generates a random 'WhichPlayer' value.
  random :: R.RandomGen g => g -> (WhichPlayer, g)
  random gen = case R.random @Bool gen of
    (True , gen2) -> (FirstPlayer, gen2)
    (False, gen2) -> (SecondPlayer, gen2)

  randomR
    :: R.RandomGen g => (WhichPlayer, WhichPlayer) -> g -> (WhichPlayer, g)
  randomR _ = R.random

-- | The function toggles a 'WhichPlayer'.
nextToPlay :: WhichPlayer -> WhichPlayer
nextToPlay FirstPlayer  = SecondPlayer
nextToPlay SecondPlayer = FirstPlayer

-- | A type describing an in-progress game's state.
data InProgressGameState = MkInProgressGameState
  { inProgressGrid         :: Grid
  , inProgressOnMove       :: WhichPlayer
  , inProgressFirstPlayer  :: Player
  , inProgressSecondPlayer :: Player
  }
  deriving (Eq, Show)

-- | A data type for a game in a state past initialisation. Such a game
-- can be in these states:
--
-- 1. In progress
-- 2. Finished, but it is a draw
-- 3. Finished and there is a winner
data GameStatus
  = InProgress InProgressGameState
  | Draw Grid
  | Victory WhichPlayer Grid
  deriving (Eq, Show)

instance ToJSON GameStatus where
  toJSON = \case
    InProgress (MkInProgressGameState g onMove first second) -> object
      [ statusStr .= String "in-progress"
      , "on-move" .= onMove
      , "grid" .= g
      , "first-player" .= playerName first
      , "second-player" .= playerName second
      ]
    Draw g -> object [statusStr .= String "draw", "grid" .= g]
    Victory igrac g ->
      object [statusStr .= String "victory", "winner" .= igrac, "grid" .= g]
    where statusStr = "status"

instance ToSample GameStatus where
  toSamples :: Proxy GameStatus -> [(Text, GameStatus)]
  toSamples _ =
    [ ("When the game is in progress"          , InProgress inProgressStatus)
    , ("When the grid is full and it is a draw", Draw drawGrid)
    , ("When there is a winner"                , Victory FirstPlayer fullGrid)
    ]
   where
    inProgressStatus :: InProgressGameState
    inProgressStatus = MkInProgressGameState
      { inProgressGrid         = g1
      , inProgressOnMove       = FirstPlayer
      , inProgressFirstPlayer = mkPlayer "Jane" "ipiwkzeatpadlgmnqedhiyxleqzmvl"
      , inProgressSecondPlayer = mkPlayer "John"
                                          "dhiyxleqzmvlipiwkzeatpadlgmnqe"
      }
    mkPlayer :: String -> String -> Player
    mkPlayer name token = MkPlayer (MkPlayerName name) (MkPlayerToken token)

-- | The function for a given token, a game ID and a game status checks
-- if the token belongs to one of the game's players. A match exists only
-- for in-progrss games. If a game is not in progress or there is no match,
-- the function returns @Nothing@, otherwise it returns an ordered triple
-- consisting of a game ID, a player tag and an in-progress game state.
checkGame
  :: PlayerToken
  -> (GameId, GameStatus)
  -> Maybe (GameId, WhichPlayer, InProgressGameState)
checkGame tok (gameId, status) = case status of
  InProgress st@(MkInProgressGameState _grid _onMove first second) ->
    if tok == playerToken first
      then Just (gameId, FirstPlayer, st)
      else if tok == playerToken second
        then Just (gameId, SecondPlayer, st)
        else Nothing
  _ -> Nothing
