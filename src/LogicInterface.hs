module LogicInterface where

import           Control.Applicative            ( Alternative((<|>)) )
import           Control.Monad                  ( when )
import           Data.Foldable                  ( foldl' )
import           Data.Functor                   ( void )
import           Numeric.Natural                ( Natural )
import           Polysemy                       ( Member
                                                , Members
                                                , Sem
                                                )
import           Polysemy.Error                 ( note
                                                , throw
                                                , Error
                                                )
import           Polysemy.Input

import           Config                         ( Config(..) )
import           Polysemy.Random                ( Random
                                                , random
                                                )
import           KVS                            ( KVS
                                                , listAllKVS
                                                , getKVS
                                                , insertKVS
                                                , deleteKVS
                                                , sizeKVS
                                                , setKVS
                                                )
import           LogicInterface.Types
import           Logic                          ( Disc(Blue, Red)
                                                , ColumnIndex(..)
                                                , emptyGrid
                                                , insertIntoGrid
                                                , isGridFull
                                                , hasWon
                                                )


-- | Fetches a list of all open games.
allOpenGames :: Member (KVS GameId Player) r => Sem r [OpenGameInfo]
allOpenGames =
  fmap
      (\(gameId, player) -> MkOpenGameInfo
        { openGameInfoGameId      = gameId
        , openGameInfoFirstPlayer = playerName player
        }
      )
    <$> listAllKVS

data NewGameError
  = NewEmptyPlayerName
  | NewTakenName PlayerName
  | NewTooManyOpenGames Natural
  deriving (Eq, Show)

-- | Creates a new half initialised game with one player.
newGame
  :: ( Members
         '[ KVS GameId Player
          , Error NewGameError
          , Random
          , Input Config
          ]
         r
     )
  => PlayerName -- ^ The name of a player creating the game
  -> Sem r HalfInitialisedGame -- ^ A newly created open game
newGame (isNameEmpty -> True)    = throw NewEmptyPlayerName
newGame (dropWhitespace -> name) = do
  maxNumOfGames     <- fmap configMaxOpenGames input
  currentNumOfGames <- sizeKVS
  when (currentNumOfGames >= maxNumOfGames)
    . void . throw . NewTooManyOpenGames $ maxNumOfGames
  playersWaiting <- fmap (fmap openGameInfoFirstPlayer) allOpenGames
  when (name `elem` playersWaiting)
    . void . throw . NewTakenName $ name
  gameId <- random
  token  <- random
  let player   = MkPlayer name token
      halfGame = MkHalfInitialisedGame
        { halfInitialisedGameGameId = gameId
        , halfInitialisedGamePlayer = player
        }
  insertKVS gameId player
  pure halfGame

data FinaliseNewGameError
  = FinaliseEmptyPlayerName
  | FinaliseUnknownGame GameId
  | FinaliseTakenName PlayerName GameId
  deriving (Eq, Show)

-- | Finalises the initialisation of an open game.
finaliseNewGame
  :: ( Members
         '[ KVS GameId Player
          , KVS GameId GameStatus
          , Error FinaliseNewGameError
          , Random
          ]
         r
     )
  => PlayerName
  -> GameId
  -> Sem r PlayerToken
finaliseNewGame (isNameEmpty -> True) _ =
  throw FinaliseEmptyPlayerName
finaliseNewGame secondPlayer' gameId = do
  mGame <- getKVS @GameId @Player gameId
  firstPlayer <- note (FinaliseUnknownGame gameId) mGame
  let secondPlayer = dropWhitespace secondPlayer'
  when (playerName firstPlayer == secondPlayer)
    . void . throw $ FinaliseTakenName secondPlayer gameId
  secondToken <- random
  whichPlayer <- random
  let initialisedGame = MkInProgressGameState
        { inProgressGrid         = emptyGrid
        , inProgressOnMove       = whichPlayer
        , inProgressFirstPlayer  = firstPlayer
        , inProgressSecondPlayer = MkPlayer secondPlayer secondToken
        }
  insertKVS gameId (InProgress initialisedGame)
  deleteKVS @GameId @Player gameId
  pure secondToken

data DiscInsertionError
  = InsertNonExistingColumn ColumnIndex
  | InsertInvalidToken PlayerToken
  | InsertNotOnMove
      WhichPlayer -- ^ The first argument 'WhichPlayer' is a player that
                  -- attempted to play
      WhichPlayer -- ^ The second argument 'WhichPlayer' is a player who
                  -- is on move
  | InsertFullColumn ColumnIndex
  deriving (Eq, Show)

-- | Inserts a disc into an existing game. If the insertion move was valid,
-- the function returns a new game status, otherwise it returns a
-- corresponding 'DiscInsertionError' error.
insertDisc
  :: (Members '[KVS GameId GameStatus, Error DiscInsertionError] r)
  => DiscInsertion
  -> Sem r GameStatus
insertDisc i | isOutOfGrid i =
  throw . InsertNonExistingColumn . discInsertionColumn $ i
insertDisc i = do
  allGames <- listAllKVS
  let token = discInsertionPlayerToken i
      mGame = foldl' (<|>) Nothing (fmap (checkGame token) allGames)
  (gameId, whichPlayer, st) <- note (InsertInvalidToken token) mGame
  let grid   = inProgressGrid st
      onMove = inProgressOnMove st
  when (whichPlayer /= onMove)
    . void . throw $ InsertNotOnMove whichPlayer onMove
  let (disc, funIsItVictory) = case onMove of
        FirstPlayer  -> (Red, hasWon Red)
        SecondPlayer -> (Blue, hasWon Blue)
      col         = getColumnIndex . discInsertionColumn $ i
      updatedGrid = insertIntoGrid disc (fromIntegral col) grid
  when (updatedGrid == grid)
    . void . throw . InsertFullColumn . discInsertionColumn $ i
  let status
        | funIsItVictory updatedGrid (fromIntegral col) = Victory
            onMove
            updatedGrid
        | isGridFull updatedGrid = Draw updatedGrid
        | otherwise = InProgress
            (st { inProgressGrid   = updatedGrid
                , inProgressOnMove = nextToPlay onMove
                }
            )
  void (setKVS gameId status)
  pure status

newtype GameStatusError = StatusNonExistingGame GameId
  deriving (Eq, Show)

-- | Returns the status of the game given by the 'GameId'.
-- If a game does not exist, the function returns an error.
gameStatus
  :: (Members '[KVS GameId GameStatus, Error GameStatusError] r)
  => GameId
  -> Sem r GameStatus
gameStatus gameId = getKVS gameId >>= \case
  Nothing -> throw (StatusNonExistingGame gameId)
  Just s  -> pure s
