module InterfaceImplementation.InMemoryKVS where

import qualified Data.Map.Strict               as M
import           Data.IORef                     ( IORef )
import           Polysemy                       ( Member
                                                , Sem
                                                , Embed
                                                , interpret
                                                )
import           Polysemy.AtomicState           ( AtomicState
                                                , atomicGet
                                                , atomicModify
                                                , runAtomicStateIORef
                                                )

import           KVS                            ( KVS(..) )

runKVSInMapState
  :: (Ord k, Member (AtomicState (M.Map k v)) r)
  => Sem (KVS k v : r) a
  -> Sem r a
runKVSInMapState = interpret
  (\case
    ListAllKVS    -> fmap M.toList atomicGet
    GetKVS k      -> fmap (M.lookup k) atomicGet
    InsertKVS k v -> atomicModify (M.insert k v)
    DeleteKVS k   -> atomicModify (M.delete k)
  )

runKVSInMemory
  :: (Ord k, Member (Embed IO) r)
  => IORef (M.Map k v)
  -> Sem (KVS k v : AtomicState (M.Map k v) : r) a
  -> Sem r a
runKVSInMemory ioRefMap = runAtomicStateIORef ioRefMap . runKVSInMapState
