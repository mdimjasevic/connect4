module InterfaceImplementation.App
  ( createApp
  ) where

import           Prelude                 hiding ( unwords )
import           Control.Monad.Except           ( ExceptT(ExceptT) )
import           Data.ByteString.Lazy           ( ByteString
                                                , fromStrict
                                                )
import           Data.Either.Extra              ( mapLeft )
import           Data.Function                  ( (&) )
import           Data.IORef                     ( IORef )
import qualified Data.Map.Strict               as M
import           Data.Text                      ( pack
                                                , Text
                                                , unwords
                                                )
import           Data.Text.Encoding             ( encodeUtf8 )
import           Polysemy                       ( runM
                                                , Sem
                                                )
import           Polysemy.Embed.Type            ( Embed )
import           Polysemy.Error                 ( Error
                                                , mapError
                                                , runError
                                                )
import           Polysemy.Input                 ( Input
                                                , runInputConst
                                                )
import           Polysemy.AtomicState           ( AtomicState )
import           Servant.Server                 ( Application
                                                , serve
                                                , ServerT
                                                , Handler(Handler)
                                                , hoistServer
                                                , ServerError(errBody)
                                                , err400
                                                , err409
                                                )
import qualified System.Random                 as R

import           Config                         ( Config )
import           InterfaceImplementation.InMemoryKVS
                                                ( runKVSInMemory )
import           Polysemy.Random                ( Random
                                                , runRandom
                                                )
import           KVS                            ( KVS )
import           LogicInterface                 ( NewGameError(..)
                                                , FinaliseNewGameError(..)
                                                , DiscInsertionError(..)
                                                , GameStatusError(..)
                                                )
import           LogicInterface.Types           ( GameId(..)
                                                , PlayerName(..)
                                                , Player
                                                , PlayerToken(..)
                                                , GameStatus
                                                )
import           WebAPI                         ( GameAPI
                                                , api
                                                , gameServer
                                                )

-- | An umbrella application error data type that is a sum of all possible
-- errors that can be thrown in the 'LogicInterface' module.
data AppError
  = AppErrorNew NewGameError
  | AppErrorFinalise FinaliseNewGameError
  | AppErrorInsert DiscInsertionError
  | AppErrorStatus GameStatusError

toBS :: Text -> ByteString
toBS = fromStrict . encodeUtf8

textShow :: Show a => a -> Text
textShow = pack . show

-- | The function converts an application error into a server error.
-- It returns a valid value intact.
handleError :: Either AppError a -> Either ServerError a
handleError = mapLeft go where
  go :: AppError -> ServerError
  go (AppErrorNew      e) = handleNew e
  go (AppErrorFinalise e) = handleFinalise e
  go (AppErrorInsert   e) = handleInsert e
  go (AppErrorStatus   e) = handleStatus e

  handleNew :: NewGameError -> ServerError
  handleNew NewEmptyPlayerName =
    err400 { errBody = toBS "The first player's name cannot be empty!" }
  handleNew (NewTakenName name) = err409
    { errBody = toBS
                  (unwords
                    [ "The first player's name"
                    , pack (getPlayerName name)
                    , "already exists in the list of open games!"
                    ]
                  )
    }
  handleNew (NewTooManyOpenGames m) =
    err409 { errBody = toBS ("There are too many open games: " <> textShow m) }

  handleFinalise :: FinaliseNewGameError -> ServerError
  handleFinalise FinaliseEmptyPlayerName =
    err400 { errBody = toBS "The second player's name cannot be empty!" }
  handleFinalise (FinaliseUnknownGame gameId) = err409
    { errBody = toBS
      (unwords ["There is no game with an ID", pack (getGameId gameId) <> "."])
    }
  handleFinalise (FinaliseTakenName name gameId) = err409
    { errBody = toBS
                  (unwords
                    [ "It is not possible to finalise a game with an ID"
                    , pack (getGameId gameId)
                    , "because the first player is already using the name"
                    , pack (getPlayerName name) <> "."
                    ]
                  )
    }

  handleInsert :: DiscInsertionError -> ServerError
  handleInsert (InsertNonExistingColumn i) = err400
    { errBody = toBS
      (unwords ["A column index", textShow i, "is out of grid bounds."])
    }
  handleInsert (InsertInvalidToken token) = err409
    { errBody = toBS
      (unwords ["Token", pack (getPlayerToken token), "is not valid."])
    }
  handleInsert (InsertNotOnMove attempted onMove) = err409
    { errBody = toBS
                  (unwords
                    [ "It is"
                    , textShow onMove <> "'s move, yet"
                    , textShow attempted
                    , "attempted to play."
                    ]
                  )
    }
  handleInsert (InsertFullColumn i) = err400
    { errBody = toBS (unwords ["Column", textShow i, "is already full."])
    }
  handleStatus :: GameStatusError -> ServerError
  handleStatus (StatusNonExistingGame gameId) = err400
    { errBody = toBS
      (unwords
        ["A non-existing game with an ID", pack (getGameId gameId) <> "."]
      )
    }

-- | The function for a given game configuration and references to two maps
-- giving the state of the server and a random value generator creates an
-- application according to the Web Application Interface (WAI).
createApp
  :: Config
  -> IORef (M.Map GameId Player)
  -> IORef (M.Map GameId GameStatus)
  -> IORef R.StdGen
  -> IO Application
createApp conf playerMap statusMap gen =
  pure (serve api (liftServer conf playerMap statusMap gen))

liftServer
  :: Config
  -> IORef (M.Map GameId Player)
  -> IORef (M.Map GameId GameStatus)
  -> IORef R.StdGen
  -> ServerT GameAPI Handler
liftServer conf playerMap statusMap gen =
  hoistServer api (interpretServer conf playerMap statusMap gen) gameServer

-- | The list of all effects used in the game
type EffectList
  = '[ Input Config
     , KVS GameId Player
     , AtomicState (M.Map GameId Player)
     , KVS GameId GameStatus
     , AtomicState (M.Map GameId GameStatus)
     , Error NewGameError
     , Error FinaliseNewGameError
     , Error DiscInsertionError
     , Error GameStatusError
     , Error AppError
     , Random
     , Embed IO
     ]

-- | A natural transformation taking a computation from the 'Sem' monad to
-- the 'Handler' monad.
interpretServer
  :: Config
  -> IORef (M.Map GameId Player)
  -> IORef (M.Map GameId GameStatus)
  -> IORef R.StdGen
  -> Sem EffectList a
  -> Handler a
interpretServer conf playerMap statusMap gen sem =
  sem
    & runInputConst conf
    & runKVSInMemory playerMap
    & runKVSInMemory statusMap
    & mapError AppErrorNew
    & mapError AppErrorFinalise
    & mapError AppErrorInsert
    & mapError AppErrorStatus
    & runError @AppError
    & runRandom gen
    & runM
    & liftToHandler

liftToHandler :: IO (Either AppError a) -> Handler a
liftToHandler = Handler . ExceptT . fmap handleError
