-- Based on https://github.com/polysemy-research/polysemy-zoo/blob/ac99fdcca3dd7b445f2b14e8f6e8ab7031d9ed34/src/Polysemy/Random.hs.
-- The difference is in using an 'AtomicState' supporting concurrency,
-- instead of an implementation via 'State'.
{-# LANGUAGE TemplateHaskell #-}

module Polysemy.Random
  ( -- * Effect
    Random(..)

    -- * Actions
  , random
  , randomR

    -- * Interpretations
  , runRandom
  , runRandomIO
  ) where

import           Data.IORef                     ( IORef
                                                , newIORef
                                                )
import           Polysemy                       ( Member
                                                , Sem
                                                , embed
                                                , Embed
                                                , reinterpret
                                                , makeSem
                                                )
import           Polysemy.AtomicState           ( atomicGets
                                                , atomicPut
                                                , runAtomicStateIORef
                                                )
import qualified System.Random                 as R

------------------------------------------------------------------------------
-- | An effect capable of providing 'R.Random' values.
data Random m a where
  Random ::R.Random x => Random m x
  RandomR ::R.Random x => (x, x) -> Random m x

makeSem ''Random


------------------------------------------------------------------------------
-- | Run a 'Random' effect with an explicit 'R.RandomGen'.
runRandom
  :: forall q r a
   . (R.RandomGen q, Member (Embed IO) r)
  => IORef q
  -> Sem (Random ': r) a
  -> Sem r a
runRandom ioRefQ = runAtomicStateIORef ioRefQ . reinterpret
  (\case
    Random -> do
      ~(a, q') <- atomicGets @q R.random
      atomicPut q'
      pure a
    RandomR r -> do
      ~(a, q') <- atomicGets @q $ R.randomR r
      atomicPut q'
      pure a
  )
{-# INLINE runRandom #-}


------------------------------------------------------------------------------
-- | Run a 'Random' effect by using the 'IO' random generator.
runRandomIO :: Member (Embed IO) r => Sem (Random ': r) a -> Sem r a
runRandomIO m = do
  q <- embed @IO $ R.newStdGen >>= newIORef
  runRandom q m
{-# INLINE runRandomIO #-}
