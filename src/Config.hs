module Config where

import           Numeric.Natural                ( Natural )

-- | Global application configuration
data Config = MkConfig
  { configMaxOpenGames :: Natural -- ^ The maximum number of open games
  }
